package com.zuitt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class WDC043_S2_A2 {

    public static void main(String[] args){

       int[] primeNumbers = new int[5];
       primeNumbers[0] = 2;
       primeNumbers[1] = 3;
       primeNumbers[2] = 5;
       primeNumbers[3] = 7;
       primeNumbers[4] = 11;

       System.out.println("first prime number is: " + primeNumbers[0]);
       System.out.println("second prime number is: " + primeNumbers[1]);
       System.out.println("third prime number is: " + primeNumbers[2]);
       System.out.println("fourth prime number is: " + primeNumbers[3]);
       System.out.println("fifth prime number is: " + primeNumbers[4]);


       //friends
       ArrayList<String> friends = new ArrayList<>(Arrays.asList("John", "Jane", "Chloe", "Zoey")) ;
       System.out.println("My friends are: " + (friends));

       //inventory

        HashMap<String,Integer> inventory = new HashMap<>(){
            {
                put("Toothpaste", 15);
                put("Toothbrush", 20);
                put("Soap", 12);
            }
        };
        System.out.println(inventory);

    }
}
