package com.zuitt;

import java.util.Scanner;

public class WDC043_S2_A1 {

    public static void main(String[] args) {

        Scanner yearScanner = new Scanner(System.in);
        System.out.println("Input a year: ");

        int year = yearScanner.nextInt();

        if (year % 4 == 0) {
            System.out.println( year + " is a leap year");
        } else{
            System.out.println(year + " is not a leap year");
        }
    }
}
