package com.zuitt.example.Array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    //Java Collection
    // are single unit of objects
    //useful for manipulating relevant pieces of data that can be used in different situations, more commonly with loops

        public static void main(String[] args){

            //Arrays
                // In Java, arrays are container of values of the same type given a predefined amount values
                // Java arrays are more rigid, once the size and data type are defined, they can no longer be change

                // Array Declaration

                //datatype[] identifier = new dataType[numOfElements];
                // "[]" indicates that data type should be able to hold multiple values
                //the "new" keyword is used for non-primitive data types and to tell Java to create the said var
                // [numOfElements] - number of elements

                int[] intArray = new int[5];
                intArray[0] = 200;
                intArray[1] = 3;
                intArray[2] = 25;
                intArray[3] = 50;
                intArray[4] = 99;
                //intArray[5] = 100; out of bounds

                System.out.println(intArray); // This will return the memory address of the array

                //to print the intArray, we need to import the array class and use the .toString() method
                // this will convert  the array as a string in the terminal
                System.out.println(Arrays.toString(intArray));

                //Array declaration with initialization
                //dataType[] identifier = {elementA, elementB, elementC, ....};
                //the compiler automatically specifies the size by counting the number of elements in the array

                String[] names = {"John", "Jane", "Joe"};

                System.out.println(Arrays.toString(names));

                //sample java array method
                //sort
                Arrays.sort(intArray);
                System.out.println("Orders of items after sort(): "+ Arrays.toString(intArray));

                //MultiDimensional Array
                //two dimensional array, can be describe by two lengths nested within each other,like matrix
                //first length is "row", second length is "column"

                String[][] classroom = new String[3][3];

                //first row
                classroom[0][0] = "Naruto";
                classroom[0][1] = "Sasuke";
                classroom[0][2] = "Sakura";

                //second row
                classroom[1][0] = "Linny";
                classroom[1][1] = "Tuck";
                classroom[1][2] = "Ming-ming";

                //Third row
                classroom[2][0] = "Harry";
                classroom[2][1] = "Ron";
                classroom[2][2] = "Hermione";

                //deepToString is used for multidimensional arrays
                System.out.println(Arrays.deepToString(classroom));

                //Arraylists
                        //are resizable arrays, wherein elements can be added or removed whenever it is needed
                //Syntax
                        //ArrayList<T> identifier = new ArrayList<T>();
                        //"<T>" is used to specify that the list can only have one type of objects in a collection
                //ArrayList cannot hold primitive data types,"java wrapper classes" provide a way to use this types of objects
                //wrapper classes = object version of primitive data types with methods

                // declare an ArrayList

                //      ArrayList<int> numbers = new ArrayList<int>();

                //ArrayList<Integer> numbers = new ArrayList<Integer>();
                //ArrayList<String> students = new ArrayList<String>();

                //add elements
                //arrayListName.add(element);

                //students.add("Cardo");
                //students.add("Luffy");
                //System.out.println(students);

                //Declare an ArrayList with values
                ArrayList<String> students = new ArrayList<>(Arrays.asList("Jane", "Mike"));
                students.add("Cardo");
                students.add("Luffy");
                System.out.println(students);

                //access element
                //arrayListName.get(index);
                System.out.println(students.get(3));

                //add an element on a specific index
                //ArrayListName.add(index, element);
                students.add(0,"CardiB");
                System.out.println(students.get(0));
                System.out.println(students);

                //updating an element
                //arrayListName.set(index,element)
                students.set(1,"Tom");
                System.out.println(students);

                //removing a specific elements
                //arrayListName.remove(index);
                students.remove(1);
                System.out.println(students);

                //remove all elements
                students.clear();
                //students.removeAll(students);
                System.out.println(students);

                //getting the arrayList size
                System.out.println(students.size());

                //hashmaps
                        //most objects in Java are defined and are instantiations of class that contains a proper set of properties and methods
                //there might be use cases where this is not appropriate, or you may simply want to store a collection of data in key-value pairs.
                //in Java "Keys" are also referred as "fields"
                //wherein the values are accessed by the fields
                        //Syntax
                        //HashMap<dataTypeField, dataTypeValue> identifier = new HashMap<>();
                //declare hashmap






                //declare hashmaps with initialization
                HashMap<String,String> jobPosition = new HashMap<>(){

                        {
                                put("Teacher","Cee");
                                put("Web Developer", "Peter Parker");
                        }
                };

                //add element
                //hashMapName.put(<fieldName>, <value>);
                jobPosition.put("Dreamer","Morpheus");
                jobPosition.put("Police", "Cardi");
                System.out.println(jobPosition);

                //access element
                //hashMapName.get("fieldName");
                System.out.println(jobPosition.get("Police"));

                //update the value of element
                //hashMapName.replace("fieldNameToChange", "newValue");
                jobPosition.replace("Dreamer","Persephone");
                System.out.println(jobPosition);

                //remove element
                jobPosition.remove("Dreamer");
                System.out.println(jobPosition);

                //retrieve hashmaps keys in array
                System.out.println(jobPosition.keySet());

                //retrieve hashmaps values in array
                System.out.println(jobPosition.values());

                //remove all elements in hashmap
                jobPosition.clear();
                System.out.println(jobPosition);
        }

}
