package com.zuitt.example.Array;

import java.util.Scanner;

public class SelectionControl {

    public static void main(String[] args) {
        //Java Operators
        //Arithmetic Operators
        // +,-,*,/,%
        //comparison
        // >,<,>=,<=,==,!=
        //logical
        //&&,||, !
        //Assignment
        // =

        //Control Structure in Java
        //Statement allows s to manipulate the flow of code depending on evaluation of the condition
        //Syntax;
        /*
         if(condition){

         }
         else{
         }
        **/

        //mini activity
        int num1 = 36;

        if (num1 % 5 == 0) {
            System.out.println(num1 + "divisible by 5");
        } else {
            System.out.println(num1 + " not divisible by 5");

        }
//        short-circuiting
//        a technique that is applicable only to the AND & OR operators wherein if statements or others controls structures can exit early by ensuring safety of operation or efficiency
//        right hand operand is not evaluated
//        OR operator
//        (true ||..) = true
//        and operator
//        (false &&..) =false


        int x = 15;
        int y = 0;

        if(y !=0 && x/y == 0){
            System.out.println("Result is: " + x/y);
        } else{
            System.out.println("This will only run because of short circuiting!");
        }

        //Ternary Operator

        int num2 = 24;
        Boolean result = (num2>0)?true:false;
        System.out.println(result);

        //Switch Cases
        Scanner numberScanner = new Scanner(System.in);
        System.out.println("Enter a number: ");

        int directionValue = numberScanner.nextInt();

        switch (directionValue){
            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default:
                System.out.println("invalid");
        }
    }

}
